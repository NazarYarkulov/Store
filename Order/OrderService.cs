﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StoreCache;
using StoreDb;
using StoreDb.Entities;
using StoreDb.Enums;

namespace Order
{
    public class OrderService : IOrderService
    {
        private readonly IStoreDbContext _dbContext;
        private readonly ICache<StoreDb.Entities.Order> _cache;

        public OrderService(IStoreDbContext dbContext, ICache<StoreDb.Entities.Order> cache)
        {
            _dbContext = dbContext;
            _cache = cache;
        }

        public async Task CreateAsync(int userId, ICollection<Good> products, CancellationToken token = default)
        {
            _cache.RemoveIfExist(CacheKeys.CurrentOrder);

            var lastOrderId = await _dbContext.Orders
                .Where(o => o.UserId == userId)
                .Select(o => o.Id)
                .LastOrDefaultAsync(token);

            var order = new StoreDb.Entities.Order
            {
                Id = lastOrderId + 1,
                UserId = userId,
                Products = products,
                Status = OrderStatus.New,
                LastChangesDate = DateTime.UtcNow
            };

            _dbContext.Orders.Add(order);
            await _dbContext.SaveChangesAsync(token);

            _cache.AddToCache(CacheKeys.CurrentOrder, order);
        }

        public async Task<IEnumerable<StoreDb.Entities.Order>> GetAllAsync(int userId, CancellationToken token = default) =>
            await _dbContext.Orders.Where(o => o.UserId == userId).ToListAsync(token);

        public async Task<IEnumerable<StoreDb.Entities.Order>> GetAllAsync(CancellationToken token = default) =>
            await _dbContext.Orders.ToListAsync(token);

        public async Task AddAsync(StoreDb.Entities.Order order, ICollection<Good> products,
            CancellationToken token = default)
        {
            if (order.Products == null)
            {
                order.Products = products;
            }
            else
            {
                foreach (var product in products)
                {
                    order.Products.Add(product);
                }
            }

            order.LastChangesDate = DateTime.UtcNow;
            await _dbContext.SaveChangesAsync(token);
            _cache.AddToCache(CacheKeys.CurrentOrder, order);
        }

        public async Task ChangeStatusAsync(StoreDb.Entities.Order order, OrderStatus status,
            CancellationToken token = default)
        {
            order.Status = status;
            order.LastChangesDate = DateTime.UtcNow;

            await _dbContext.SaveChangesAsync(token);

            _cache.AddToCache(CacheKeys.CurrentOrder, order);
        }
    }
}
