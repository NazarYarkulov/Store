﻿using Microsoft.Extensions.DependencyInjection;

namespace Order
{
    public static class Bootstrap
    {
        public static IServiceCollection AddOrder(this IServiceCollection services)
        {
            services.AddTransient<IOrderService, OrderService>();
            return services;
        }
    }
}
