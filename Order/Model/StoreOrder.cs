﻿using System;
using StoreDb.Enums;

namespace Order.Model
{
    public class StoreOrder
    {
        public int Id { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime LastChangesDate { get; set; }
    }
}
