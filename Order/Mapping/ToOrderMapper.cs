﻿using Order.Model;

namespace Order.Mapping
{
    public static class ToOrderMapper
    {
        public static StoreOrder ToStoreOrder(this StoreDb.Entities.Order order) =>
         new StoreOrder
         {
             Id = order.Id,
             Status = order.Status,
             LastChangesDate = order.LastChangesDate
         };
    }
}
