﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StoreDb.Entities;
using StoreDb.Enums;

namespace Order
{
    public interface IOrderService
    {
        Task CreateAsync(int userId, ICollection<Good> products, CancellationToken token = default);
        Task<IEnumerable<StoreDb.Entities.Order>> GetAllAsync(int userId, CancellationToken token = default);
        Task<IEnumerable<StoreDb.Entities.Order>> GetAllAsync(CancellationToken token = default);
        Task AddAsync(StoreDb.Entities.Order order, ICollection<Good> products, CancellationToken token = default);
        Task ChangeStatusAsync(StoreDb.Entities.Order order, OrderStatus status, CancellationToken token = default);
    }
}
