﻿using System;
using System.Linq;
using System.Threading.Tasks;
using StoreCache;
using StoreClient.Views.Helpers;
using StoreDb;
using StoreDb.Entities;
using User;
using User.Model;

namespace StoreClient.Views
{
    internal class UserView : View<StoreUser>
    {
        private readonly IUserService _userService;
        private readonly ICache<StoreUser> _cache;
        public UserView(IUserService userService, ICache<StoreUser> cache)
        {
            _userService = userService;
            _cache = cache;
            Headers = new[]
            {
                new TableColumn { Value = "Ідентифікатор", Width = 15 },
                new TableColumn { Value = "Логін", Width = 50 },
                new TableColumn { Value = "Функціональні ролі", Width = 150 },
            };
        }

        public void Logout()
        {
            _userService.Logout();
            Console.WriteLine("Ви успішно вийшли з акаунту.");
        }
        public async Task Login()
        {
            var inputData = ReadLoginAndPassword();
            var user = await _userService.LoginAsync(inputData.Login, inputData.Password);

            if (user != null)
            {
                Console.WriteLine($"Вхід виконано успішно. Вітаємо {user.Username}");
            }
            else
            {
                Console.WriteLine("Не вдалося війти. Неправильний логін або пароль");
            }
        }
        public async Task Register()
        {
            var inputData = ReadLoginAndPassword();
            var user = await _userService.RegisterAsync(inputData.Login, inputData.Password);

            if (user != null)
            {
                Console.WriteLine($"Успішна регістрація. Вітаємо {user.Username}");
            }
        }
        public async Task ChangeUser()
        {
            Console.WriteLine("Введіть нові данні");
            var inputData = ReadLoginAndPassword();

            var oldUsername = _cache.Get(CacheKeys.CurrentUser).Username;
            await _userService.ChangeUserInfoAsync(new ChangeUserModel
            {
                IsSelfChange = true,
                OldUsername = oldUsername,
                Username = inputData.Login,
                Password = inputData.Password
            });

            Console.WriteLine("Дані змінено успішно");
        }
        public async Task SeeAndChangeUser()
        {
            Console.WriteLine("Досутпні користувачі");
            var users = await _userService.GetAsync(new Role { Name = AvailableRoles.User });

            ShowAll(users);

            Console.WriteLine(Environment.NewLine + "Введіть логін користувача для зміни його данних");
            var username = Console.ReadLine();

            if (users.Any(u => u.Username == username))
            {
                Console.WriteLine("Введіть нові дані для користувача");

                var inputData = ReadLoginAndPassword();
                await _userService.ChangeUserInfoAsync(new ChangeUserModel
                {
                    OldUsername = username,
                    Username = inputData.Login,
                    Password = inputData.Password,
                    IsSelfChange = false
                });

                Console.WriteLine("Дані змінено успішно");
            }
            else
            {
                Console.WriteLine("Користувача з таким логіном не існує");
            }
        }

        private (string Login, string Password) ReadLoginAndPassword()
        {
            Console.WriteLine("Введіть логін");
            var login = Console.ReadLine();

            Console.WriteLine("Введіть пароль");
            var password = Console.ReadLine();

            return (login, password);
        }
    }
}
