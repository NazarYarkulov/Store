﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Product;
using Product.Mapping;
using StoreClient.Views.Helpers;
using StoreDb.Entities;

namespace StoreClient.Views
{
    internal class ProductView : View<Product.Model.Product>
    {
        private readonly IProductService _productService;

        public ProductView(IProductService productService)
        {
            _productService = productService;
            Headers = new[]
            {
                new TableColumn { Value =  "Ідентифікатор", Width = 15 },
                new TableColumn { Value =  "Назва", Width = 40 },
                new TableColumn { Value = "Категорія",Width = 30 },
                new TableColumn { Value = "Опис", Width = 80 },
                new TableColumn { Value = "Ціна", Width = 30 }
            };
        }

        public async Task AllProducts()
        {
            var products = await _productService.GetAllAsync();

            if (products == null)
            {
                Console.WriteLine("Товари відсутні в магазині");
            }

            ShowAll(products?.Select(x => x.ToProduct()));
        }
        public async Task<Good> SearchProduct()
        {
            Console.WriteLine("Введіть назву товару");
            var productName = Console.ReadLine();

            var product = await _productService.GetAsync(productName);
            if (product == null)
            {
                throw new Exception("Товар не знайдено");
            }

            ShowHeader();
            Show(product.ToProduct());
            return product;
        }

        public async Task ChangeProduct()
        {
            var product = await SearchProduct();
            Console.WriteLine("Введіть нові дані для товару");
            var inputData = GetProductFromInput();

            product.Name = inputData.Name ?? product.Name;
            product.Category = inputData.Category ?? product.Category;
            product.Description = inputData.Description ?? product.Description;
            product.Price = inputData.Price == default ? product.Price : inputData.Price;

            await _productService.ChangeAsync(product);
        }

        public async Task AddProduct()
        {
            var inputData = GetProductFromInput();
            await _productService.AddAsync(inputData);
            Console.WriteLine("Товар успішно додано");
        }

        private Good GetProductFromInput()
        {
            var product = new Good();

            Console.WriteLine("Ведіть назву");
            var name = Console.ReadLine();

            Console.WriteLine("Ведіть категорію");
            var category = Console.ReadLine();

            Console.WriteLine("Ведіть опис");
            var description = Console.ReadLine();

            Console.WriteLine("Введіть ціну");
            var price = Console.ReadLine();

            return new Good
            {
                Name = name,
                Category = category,
                Description = description,
                Price = string.IsNullOrEmpty(price) ? default : Convert.ToDouble(price)
            };
        }
    }
}
