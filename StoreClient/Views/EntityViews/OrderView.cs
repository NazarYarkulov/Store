﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Order;
using Order.Mapping;
using Order.Model;
using StoreCache;
using StoreClient.Command;
using StoreClient.Views.Helpers;
using StoreDb;
using StoreDb.Entities;
using StoreDb.Enums;
using User.Model;

namespace StoreClient.Views.EntityViews
{
    internal class OrderView : View<StoreOrder>
    {
        private readonly IOrderService _orderService;
        private readonly ProductView _productView;
        private readonly ICache<StoreUser> _userCache;
        private readonly ICache<StoreDb.Entities.Order> _orderCache;

        public OrderView(
            IOrderService orderService,
            ProductView productView,
            ICache<StoreUser> userCache,
            ICache<StoreDb.Entities.Order> orderCache)
        {
            _orderService = orderService;
            _productView = productView;
            _userCache = userCache;
            _orderCache = orderCache;

            Headers = new[]
            {
                new TableColumn {Value = "Ідентифікатор", Width = 15},
                new TableColumn {Value = "Статус замовлення", Width = 40},
                new TableColumn {Value = "Дата зміни", Width = 50}
            };
        }

        public async Task CreateOrder()
        {
            Console.WriteLine("Введіть ідентифіктор товару зі списка щоб добавити його в корзину");
            await _productView.AllProducts();

            var product = await _productView.SearchProduct();
            var user = _userCache.Get(CacheKeys.CurrentUser);

            await _orderService.CreateAsync(user.Id, new List<Good> { product });
        }

        public async Task ChangeOrderStatus()
        {
            Console.WriteLine("Виберіть статус для замовлення");
            foreach (var status in Commands.GetOrderStatusCommands())
            {
                Console.WriteLine($"{(int)status.Key} - {status.Value}");
            }

            OrderStatus input = Enum.Parse<OrderStatus>(Console.ReadLine());
            var currentOrder = _orderCache.Get(CacheKeys.CurrentOrder);

            await _orderService.ChangeStatusAsync(currentOrder, input);

            Console.WriteLine("Статус змінено успішно");
        }

        public async Task OrderHistory()
        {
            var currentUserId = _userCache.Get(CacheKeys.CurrentUser);
            if (currentUserId?.Id == default)
            {
                throw new Exception();
            }

            var orders = await _orderService.GetAllAsync(currentUserId.Id);
            ShowAll(orders.Select(o => o.ToStoreOrder()));
        }

        public async Task SetGetOrderStatus()
        {
            var currentOrder = _orderCache.Get(CacheKeys.CurrentOrder);
            await _orderService.ChangeStatusAsync(currentOrder, OrderStatus.Received);

            Console.WriteLine("Статус замовлення змінено на отримано");
        }

        public async Task MakeOrder()
        {
            var user = _userCache.Get(CacheKeys.CurrentUser);
            var order = _orderCache.Get(CacheKeys.CurrentOrder);

            if (user.Roles.Any(r => r == AvailableRoles.Admin))
            {
                await AdminOrderProcess();
            }
            else
            {
                await UserOrderProcess(order);
            }
        }

        private async Task UserOrderProcess(StoreDb.Entities.Order order)
        {
            Console.WriteLine("Введіть '1' щоб додати товар '2' щоб ска'сувати замовлення");

            var inputData = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
            if (inputData == 1)
            {
                await _productView.AllProducts();
                Console.WriteLine("Виберіть ідентифікатор товару щоб його додати");

                var product = await _productView.SearchProduct();
                await _orderService.AddAsync(order, new List<Good> { product });

                Console.WriteLine("Товар до замовлення додано успішно");
            }
            else if (inputData == 2)
            {
                await _orderService.ChangeStatusAsync(order, OrderStatus.CanceledByUser);

                Console.WriteLine("Замовлення скасовано");

                _userCache.Remove(CacheKeys.CurrentOrder);
            }
        }

        private async Task AdminOrderProcess()
        {
            Console.WriteLine("Виберіть ідентифіктор замовлення для подальшої обробки");

            var orders = (await _orderService.GetAllAsync()).ToArray();

            ShowAll(orders.Select(o => o.ToStoreOrder()));

            var orderId = int.Parse(Console.ReadLine());
            var currentOrder = orders.FirstOrDefault(o => o.Id == orderId);

            if (currentOrder != null)
            {
                _orderCache.AddToCache(CacheKeys.CurrentOrder, currentOrder);
                await ChangeOrderStatus();
            }
        }
    }
}
