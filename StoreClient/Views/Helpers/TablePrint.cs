﻿using System;
using System.Text;

namespace StoreClient.Views.Helpers
{
    public static class TablePrint
    {
        private const int TableWidth = 200;
        private const int SymbolsForDot = 4;
        public static void PrintRow(params TableColumn[] columns)
        {
            var row = new StringBuilder();
            foreach (var column in columns)
            {
                if (column.Value.Length > column.Width)
                {
                    row.Append(column.Value.Truncate(column.Width - SymbolsForDot) + "... ");
                }
                else
                {
                    var tempValue = new StringBuilder(column.Value);
                    while (tempValue.Length != column.Width)
                    {
                        tempValue.Append(' ');
                    }
                    row.Append(tempValue);
                }
            }

            PrintLine();
            Console.WriteLine(row);
            PrintLine();
        }

        private static void PrintLine() => Console.WriteLine(new string('-', TableWidth));

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

    }
}
