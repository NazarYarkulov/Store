﻿namespace StoreClient.Views.Helpers
{
    public class TableColumn
    {
        public string Value { get; set; }
        public int Width { get; set; }
    }
}
