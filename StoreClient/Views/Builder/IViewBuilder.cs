﻿using System;

namespace StoreClient.Views.Builder
{
    public interface IViewBuilder
    {
        Action BuildGuest();
        Action BuildUser();
        Action BuildAdmin();

        MainView GetView();
    }
}
