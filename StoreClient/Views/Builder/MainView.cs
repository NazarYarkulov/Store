﻿using System;

namespace StoreClient.Views
{
    public class MainView
    {
        public Action Guest { get; set; }
        public Action User { get; set; }
        public Action Admin { get; set; }
    }
}
