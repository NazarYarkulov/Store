﻿using System;
using System.Collections.Generic;
using StoreClient.Command;

namespace StoreClient.Views.Builder
{
    internal class MainViewBuilder : IViewBuilder
    {
        private readonly Dictionary<CommandType, string> _commands = Commands.GetMainCommands();

        public Action BuildGuest() => () =>
        {
            PrintAllowedCommands(new[]
            {
                CommandType.AllGoods,
                CommandType.GoodsSearch,
                CommandType.Register,
                CommandType.Enter
            });
        };
        public Action BuildUser() => () =>
        {
            PrintAllowedCommands(new[]
            {
                CommandType.AllGoods,
                CommandType.GoodsSearch,
                CommandType.CreateOrder,
                CommandType.ProcessOrCancelOrder,
                CommandType.OrderHistory,
                CommandType.SetGetOrderStatus,
                CommandType.ChangeUserInfo,
                CommandType.Exit,
            });
        };
        public Action BuildAdmin() => () =>
        {
            PrintAllowedCommands(new[]
            {
                CommandType.AllGoods,
                CommandType.GoodsSearch,
                CommandType.CreateOrder,
                CommandType.ProcessOrder,
                CommandType.SeeAndChangeUser,
                CommandType.AddGood,
                CommandType.ChangeGoodInfo,
                CommandType.ChangeOrderStatus,
                CommandType.Exit,
            });
        };

        public MainView GetView() =>
            new MainView
            {
                User = BuildUser(),
                Admin = BuildAdmin(),
                Guest = BuildGuest()
            };

        private void PrintAllowedCommands(CommandType[] commands)
        {
            Console.WriteLine("Щоб вибрати операцію введіть відповідний номер та натисніть \"Enter\" ");
            foreach (var command in commands)
            {
                Console.WriteLine($"{(int)command} - {_commands[command]}");
            }
        }
    }
}
