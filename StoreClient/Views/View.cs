﻿using System.Collections.Generic;
using System.Reflection;
using StoreClient.Views.Helpers;

namespace StoreClient.Views
{
    internal class View<T> : IView<T> where T : class
    {
        public TableColumn[] Headers { get; set; }

        public void ShowAll(IEnumerable<T> items)
        {
            TablePrint.PrintRow(Headers);
            foreach (var item in items)
            {
                Show(item);
            }
        }

        public void Show(T item)
        {
            var properties = item.GetType().GetProperties();
            var values = new TableColumn[properties.Length];
            for (var idx = 0; idx < properties.Length; idx++)
            {
                values[idx] = new TableColumn
                {
                    Value = ToString(properties[idx], item),
                    Width = Headers[idx].Width
                };
            }

            TablePrint.PrintRow(values);
        }

        public void ShowHeader() => TablePrint.PrintRow(Headers);

        private string ToString(PropertyInfo propertyInfo, T item)
        {
            var value = propertyInfo.GetValue(item);
            if (value is IEnumerable<string> collection)
            {
                return string.Join(' ', collection);
            }

            return value.ToString();
        }
    }
}
