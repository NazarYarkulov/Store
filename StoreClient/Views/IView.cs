﻿using System.Collections.Generic;
using StoreClient.Views.Helpers;

namespace StoreClient.Views
{
    public interface IView<in T> where T : class
    {
        TableColumn[] Headers { get; set; }
        void ShowAll(IEnumerable<T> items);
        void Show(T item);
    }
}
