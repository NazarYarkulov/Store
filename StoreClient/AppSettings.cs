﻿using System;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Order;
using Payment;
using Product;
using StoreCache;
using StoreClient.Handler;
using StoreClient.Views;
using StoreClient.Views.Builder;
using StoreClient.Views.EntityViews;
using StoreDb;
using User;

namespace StoreClient
{
    public static class AppSettings
    {
        private static ServiceProvider _appServiceProvider;

        static AppSettings()
        {
            // For correct ukrainian symbols output
            Console.OutputEncoding = Encoding.UTF8;
            InitializeServices();
        }

        public static T GetService<T>() => _appServiceProvider.GetService<T>();

        private static void InitializeServices()
        {
            var services = new ServiceCollection();

            //Domain registration
            services.AddStoreDb();
            services.AddStoreCache();

            //Service registration
            services.AddUser();
            services.AddProduct();
            services.AddOrder();
            services.AddPayment();

            //Presentation registration
            services.AddTransient(typeof(IView<>), typeof(View<>));
            services.AddTransient<IViewBuilder, MainViewBuilder>();
            services.AddTransient<ProductView>();
            services.AddTransient<UserView>();
            services.AddTransient<OrderView>();
            services.AddTransient<GuestHandler>();
            services.AddTransient<AdminHandler>();
            services.AddTransient<UserHandler>();
            services.AddTransient<Client>();

            _appServiceProvider = services.BuildServiceProvider();
        }
    }
}
