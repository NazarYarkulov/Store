﻿using System.Collections.Generic;
using StoreDb.Enums;

namespace StoreClient.Command
{
    public static class Commands
    {
        public static Dictionary<CommandType, string> GetMainCommands() =>
            new Dictionary<CommandType, string>
            {
                {CommandType.Enter, "Вхід до інтернет-магазину з обліковим записом"},
                {CommandType.GoodsSearch, "Пошук товару за назвою"},
                {CommandType.Register, "Реєстрація опікового запису користувача"},
                {CommandType.AllGoods, "Перегляд переліку товарів"},
                {CommandType.CreateOrder, "Створення нового замовлення"},
                {CommandType.ProcessOrCancelOrder, "Оформлення замовлення або відміна"},
                {CommandType.OrderHistory, "Перегляд історії замовлень та статусу їх доставки"},
                {CommandType.SetGetOrderStatus, "Встановлення статусу замовлення «Отримано»"},
                {CommandType.ChangeUserInfo, "Зміна персональної інформації"},
                {CommandType.ProcessOrder, "Оформлення замовлення"},
                {CommandType.SeeAndChangeUser, "Перегляд та зміна персональної інформації користувачів"},
                { CommandType.AddGood, "Додавання нового товару (назва, категорія, опис, вартість)"},
                {CommandType.ChangeGoodInfo, "Зміна інформації про товар"},
                {CommandType.ChangeOrderStatus, "Зміна статусу заломлення"},
                {CommandType.Exit, "Вихід з облікового запису"},
            };

        public static Dictionary<OrderStatus, string> GetOrderStatusCommands() =>
            new Dictionary<OrderStatus, string>
            {
                {OrderStatus.New, "Нове замовлення"},
                {OrderStatus.PaymentReceived, "Отримана оплата"},
                {OrderStatus.Sent, "Відправлено"},
                {OrderStatus.Received, "Отримано"},
                {OrderStatus.Completed, "Завершено"},
            };
    }

}
