﻿namespace StoreClient.Command
{
    public enum CommandType
    {
        Enter = 1,
        GoodsSearch = 2,
        Register = 3,
        AllGoods = 4,
        CreateOrder = 5,
        ProcessOrCancelOrder = 6,
        OrderHistory = 7,
        SetGetOrderStatus = 8,
        ChangeUserInfo = 9,
        ProcessOrder = 10,
        SeeAndChangeUser = 11,
        AddGood = 12,
        ChangeGoodInfo = 13,
        ChangeOrderStatus = 14,
        Exit = 15
    };
}
