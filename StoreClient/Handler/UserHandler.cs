﻿using System.Threading.Tasks;
using StoreCache;
using StoreClient.Command;
using StoreClient.Views;
using StoreClient.Views.EntityViews;
using User;
using User.Model;

namespace StoreClient.Handler
{
    internal class UserHandler : IHandler
    {
        private readonly UserView _userView;
        private readonly ProductView _productView;
        private readonly OrderView _orderView;

        public UserHandler(UserView userView, ProductView productView, OrderView orderView)
        {
            _userView = userView;
            _productView = productView;
            _orderView = orderView;
        }

        public async Task HandleAsync(CommandType command)
        {
            switch (command)
            {
                case CommandType.AllGoods:
                    await _productView.AllProducts();
                    break;

                case CommandType.GoodsSearch:
                    await _productView.SearchProduct();
                    break;

                case CommandType.CreateOrder:
                    await _orderView.CreateOrder();
                    return;

                case CommandType.ProcessOrCancelOrder:
                    await _orderView.MakeOrder();
                    return;
                    
                case CommandType.OrderHistory:
                    await _orderView.OrderHistory();
                    break;

                case CommandType.SetGetOrderStatus:
                    await _orderView.SetGetOrderStatus();
                    break;
                    

                case CommandType.ChangeUserInfo:
                    await _userView.ChangeUser();
                    break;

                case CommandType.Exit:
                    _userView.Logout();
                    break;
            }
        }
    }
}
