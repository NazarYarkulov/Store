﻿using System.Threading.Tasks;
using StoreClient.Command;
using StoreClient.Views;
using StoreClient.Views.EntityViews;

namespace StoreClient.Handler
{
    // Facade + Strategy
    internal class AdminHandler : IHandler
    {
        private readonly UserView _userView;
        private readonly ProductView _productView;
        private readonly OrderView  _orderView;

        public AdminHandler(UserView userView, ProductView productView, OrderView orderView)
        {
            _userView = userView;
            _productView = productView;
            _orderView = orderView;
        }

        public async Task HandleAsync(CommandType command)
        {
            switch (command)
            {
                case CommandType.AllGoods:
                    await _productView.AllProducts();
                    break;

                case CommandType.GoodsSearch:
                    await _productView.SearchProduct();
                    break;

                case CommandType.CreateOrder:
                    await _orderView.CreateOrder();
                    break;

                case CommandType.ProcessOrder:
                    await _orderView.MakeOrder();
                    break;

                case CommandType.SeeAndChangeUser:
                    await _userView.SeeAndChangeUser();
                    break;

                case CommandType.AddGood:
                    await _productView.AddProduct();
                    break;

                case CommandType.ChangeGoodInfo:
                    await _productView.ChangeProduct();
                    return;

                case CommandType.ChangeOrderStatus:
                    await _orderView.ChangeOrderStatus();
                    break;

                case CommandType.Exit:
                    _userView.Logout();
                    break;
            }
        }

    }
}

