﻿using System.Threading.Tasks;
using StoreClient.Command;

namespace StoreClient.Handler
{
    public interface IHandler
    {
        Task HandleAsync(CommandType command);
    }
}
