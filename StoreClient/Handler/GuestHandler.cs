﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Product;
using StoreCache;
using StoreClient.Command;
using StoreClient.Views;
using StoreDb.Entities;
using User;
using User.Model;

namespace StoreClient.Handler
{
    internal class GuestHandler : IHandler
    {
        private readonly UserView _userView;
        private readonly ProductView _productView;

        public GuestHandler(UserView userView, ProductView productView)
        {
            _userView = userView;
            _productView = productView;
        }

        public async Task HandleAsync(CommandType command)
        {
            switch (command)
            {
                case CommandType.AllGoods:
                    await _productView.AllProducts();
                    break;
                case CommandType.GoodsSearch:
                    await _productView.SearchProduct();
                    break;
                case CommandType.Register:
                    await _userView.Register();
                    break;
                case CommandType.Enter:
                    await _userView.Login();
                    break;
            }
        }
    }
}
