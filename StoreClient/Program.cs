﻿using System;
using System.Threading.Tasks;
using StoreDb;

namespace StoreClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;
#if DEBUG
                await AppSettings.GetService<MockDataInitializer>().AddMockDataAsync();
#endif
                await AppSettings.GetService<Client>().Run();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.ForegroundColor = ConsoleColor.White;
            Environment.Exit(1);
        }
    }
}
