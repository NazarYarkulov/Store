﻿using System;
using System.Linq;
using System.Threading.Tasks;
using StoreCache;
using StoreClient.Command;
using StoreClient.Handler;
using StoreClient.Views;
using StoreClient.Views.Builder;
using StoreDb;
using User.Model;

namespace StoreClient
{
    public class Client
    {
        private readonly MainView _mainView;
        private readonly ICache<StoreUser> _userCache;
        private IHandler _handler;

        public Client(IViewBuilder viewBuilder, ICache<StoreUser> userCache)
        {
            _userCache = userCache;
            _mainView = viewBuilder.GetView();
        }

        public async Task Run()
        {
            while (true)
            {
                _handler = GetRequestHandler();
                switch (_handler)
                {
                    case AdminHandler _:
                        _mainView.Admin();
                        break;
                    case UserHandler _:
                        _mainView.User();
                        break;
                    case GuestHandler _:
                        _mainView.Guest();
                        break;
                }

                var command = (CommandType)Convert.ToInt32(Console.ReadLine());
                await _handler.HandleAsync(command);

                EndRequest();
            }
        }

        private IHandler GetRequestHandler()
        {
            if (_userCache.TryGet(CacheKeys.CurrentUser, out StoreUser user))
            {
                if (user.Roles.Any(x => x == AvailableRoles.Admin))
                {
                    return AppSettings.GetService<AdminHandler>();
                }

                return AppSettings.GetService<UserHandler>();
            }

            return AppSettings.GetService<GuestHandler>();
        }
        private void EndRequest()
        {
            Console.WriteLine("Натисніть Enter для продовження");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
