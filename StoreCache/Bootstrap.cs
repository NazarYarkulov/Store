﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace StoreCache
{
    public static class Bootstrap
    {
        public static IServiceCollection AddStoreCache(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.TryAddSingleton(typeof(ICache<>), typeof(Cache<>));
            return services;
        }
    }
}
