﻿namespace StoreCache
{
    public interface ICache<T>
    {
        public void AddToCache(string key, T item);
        public T Get(string key);
        public void Remove(string key);
        public void RemoveIfExist(string key);
        public bool TryGet(string key, out T item);
    }
}
