﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace StoreCache
{
    //Decorator
    internal class Cache<T> : ICache<T>
    {
        private readonly IMemoryCache _memoryCache;
        private readonly TimeSpan _defaultCachePeriod = TimeSpan.FromHours(2);

        public Cache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void AddToCache(string key, T item)
        {
            using var entry = _memoryCache.CreateEntry(key);
            entry.AbsoluteExpirationRelativeToNow = _defaultCachePeriod;
            entry.SetValue(item);
        }

        public void RemoveIfExist(string key)
        {
            var isExist = TryGet(key, out _);
            if (isExist)
            {
                Remove(key);
            }
        }

        public bool TryGet(string key, out T item) => _memoryCache.TryGetValue(key, out item);
        public T Get(string key) => _memoryCache.Get<T>(key);
        public void Remove(string key) => _memoryCache.Remove(key);
    }
}
