﻿namespace StoreCache
{
    public static class CacheKeys
    {
        public const string CurrentUser = "CurrentUser";
        public const string CurrentOrder = "CurrentOrderId";
    }
}
