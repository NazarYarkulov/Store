﻿using Microsoft.Extensions.DependencyInjection;

namespace Payment
{
    public static class Bootstrap
    {
        public static IServiceCollection AddPayment(this IServiceCollection services)
        {
            services.AddTransient<IPaymentService, PaymentService>();
            return services;
        }
    }
}
