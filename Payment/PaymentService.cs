﻿using System.Threading;
using System.Threading.Tasks;
using Order;
using StoreDb.Enums;

namespace Payment
{
    public class PaymentService : IPaymentService
    {
        private readonly IOrderService _orderService;

        public PaymentService(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public async Task RemitAsync(StoreDb.Entities.Order order, CancellationToken token = default)  =>
            await _orderService.ChangeStatusAsync(order, OrderStatus.PaymentReceived, token);
    }
}
