﻿using System.Threading;
using System.Threading.Tasks;

namespace Payment
{
    public interface IPaymentService
    {
        Task RemitAsync(StoreDb.Entities.Order order, CancellationToken token = default);
    }
}
