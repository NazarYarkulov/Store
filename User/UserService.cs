﻿using System;
using System.Collections.Generic;
using System.Linq;
using StoreDb;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StoreCache;
using StoreDb.Entities;
using User.Mapping;
using User.Model;

namespace User
{
    public class UserService : IUserService
    {
        private readonly IStoreDbContext _dbContext;
        private readonly ICache<StoreUser> _userCache;

        public UserService(IStoreDbContext dbContext, ICache<StoreUser> userCache)
        {
            _dbContext = dbContext;
            _userCache = userCache;
        }

        public async Task<StoreUser> LoginAsync(string username, string password, CancellationToken token = default)
        {
            var user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Username == username, token);

            if (user.Password != password)
            {
                throw new Exception();
            }

            // Because EF core currently not support eager loading in many-to-many relationships
            user.UserRoles = await _dbContext.UserRoles
                .Where(x => x.UserId == user.Id)
                .ToListAsync(token);

            var storeUser = user?.ToStoreUser();
            if (storeUser != null)
            {
                _userCache.AddToCache(CacheKeys.CurrentUser, storeUser);
            }

            return storeUser;
        }
        public async Task<StoreUser> RegisterAsync(string username, string password, CancellationToken token = default)
        {
            await ThrowIfUserExistAsync(username, token);

            var newUser = new StoreDb.Entities.User
            {
                Username = username,
                Password = password,
            };
            _dbContext.Users.Add(newUser);

            await AttachRoleAsync(newUser, AvailableRoles.User, token);

            await _dbContext.SaveChangesAsync(token);

            var storeUser = newUser.ToStoreUser();
            _userCache.AddToCache(CacheKeys.CurrentUser, storeUser);

            return storeUser;
        }
        public async Task ChangeUserInfoAsync(ChangeUserModel model, CancellationToken token = default)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Username == model.OldUsername, token);
            user.Username = model.Username;
            user.Password = model.Password;

            await _dbContext.SaveChangesAsync(token);

            if (model.IsSelfChange)
            {
                _userCache.AddToCache(CacheKeys.CurrentUser, user.ToStoreUser());
            }
        }
        public async Task<StoreUser> GetAsync(string username)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Username == username);
            return user?.ToStoreUser();
        }
        public async Task<IList<StoreUser>> GetAsync(Role role)
        {
            if (role.Id == default)
            {
                role = await _dbContext.Roles.FirstOrDefaultAsync(r => r.Name == role.Name);
            }

            var users = await _dbContext.UserRoles
                .Include(x => x.User)
                .Where(x => x.RoleId == role.Id)
                .Select(x => x.User.ToStoreUser())
                .ToListAsync();


            return users;
        }
        public void Logout() => _userCache.Remove(CacheKeys.CurrentUser);

        private async Task AttachRoleAsync(StoreDb.Entities.User user, string roleName, CancellationToken token)
        {
            var userRole = await _dbContext.Roles
                .FirstOrDefaultAsync(x => x.Name == roleName, token);

            if (userRole == null)
            {
                var role = await _dbContext.Roles.FirstOrDefaultAsync(x => x.Name == roleName, token);
                if (role == null)
                {
                    role = new Role { Name = roleName };
                    _dbContext.Roles.Add(role);
                }

                _dbContext.UserRoles.Add(new UserRole { Role = role, User = user });
            }
        }
        private async Task ThrowIfUserExistAsync(string username, CancellationToken token)
        {
            var user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Username == username, token);

            if (user != null)
            {
                throw new Exception("User already exist in database");
            }
        }
    }
}
