﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StoreDb.Entities;
using User.Model;

namespace User
{
    public interface IUserService
    {
        Task<StoreUser> LoginAsync(string username, string password, CancellationToken cancellationToken = default);
        Task<StoreUser> RegisterAsync(string username, string password, CancellationToken cancellationToken = default);
        Task ChangeUserInfoAsync(ChangeUserModel model, CancellationToken token = default);
        Task<StoreUser> GetAsync(string username);
        Task<IList<StoreUser>> GetAsync(Role role);
        void Logout();
    }
}
