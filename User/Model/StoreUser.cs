﻿using System.Collections.Generic;

namespace User.Model
{
   public class StoreUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}
