﻿using System;
using System.Collections.Generic;
using System.Text;

namespace User.Model
{
    public class ChangeUserModel
    {
        public string OldUsername { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsSelfChange { get; set; }
    }
}
