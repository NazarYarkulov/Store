﻿using Microsoft.Extensions.DependencyInjection;

namespace User
{
    public static class Bootstrap
    {
        public static IServiceCollection AddUser(this IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            return services;
        }
    }
}
