﻿using System.Collections.Generic;
using System.Linq;
using StoreDb;
using User.Model;

namespace User.Mapping
{
    public static class StoreUserMapper
    {
        public static StoreUser ToStoreUser(this StoreDb.Entities.User user)
        {
            var storeUser = new StoreUser
            {
                Id = user.Id,
                Username = user.Username,
                Roles = user.UserRoles != null
                    ? user.UserRoles.Select(ur => ur.Role.Name)
                    : new List<string> {AvailableRoles.User},
            };


            return storeUser;
        }
    }
}
