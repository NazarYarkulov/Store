using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Product;
using StoreDb;
using StoreDb.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using System.Linq;

namespace NUnitTestForConsoleStore.Tests
{
    public class ProductServiceTests
    {
        #region Init
        private ProductService _productService;
        private IList<Good> _products;
        private Mock<DbSet<Good>> _productsDbSet;
        private Mock<IStoreDbContext> _storeDbContext;
        [SetUp]
        public void Setup()
        {
            _products = GetGoods();

            _productsDbSet = MockDbSetFactory.Create(_products);

            _storeDbContext = new Mock<IStoreDbContext>();
            _productService = new ProductService(_storeDbContext.Object);

            _storeDbContext.Setup(store => store.Products).Returns(_productsDbSet.Object);
        }
#endregion

        #region Tests
        [Test]
        public async Task GetAllAsync_Test()
        {
            // arrange
            var expected = GetGoods();
            // act
            var actual = await _productService.GetAllAsync();
            // assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Test]
        public async Task GetAsync_ByName_Test()
        {
            // arrange
            var expected = GetGood(out string name);
            // act
            var actual = await _productService.GetAsync(name);
            // assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Test]
        public async Task GetAsync_ById_Test()
        {
            // arrange
            var expected = GetGood(out int Id);
            // act
            var actual = await _productService.GetAsync(Id);
            // assert
            actual.Should().BeEquivalentTo(expected);
        }
#endregion

        #region Data
        private IList<Good> GetGoods()
        {
            return new List<Good>
            {
                new Good(){Id = 1, Name = "kot" },
                new Good(){Id = 2, Name = "sobaka" },
                new Good(){Id = 3, Name = "pavel" }
            };
        }
        private Good GetGood(out string name)
        {
            name = "sobaka";
            return new Good() { Id = 2, Name = "sobaka" };
        }
        private Good GetGood(out int id)
        {
            id = 3;
            return new Good() { Id = 3, Name = "pavel" };
        }
        #endregion

    }

}