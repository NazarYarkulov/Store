﻿using Microsoft.EntityFrameworkCore;
using Moq;
using NUnitTestForConsoleStore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NUnitTestForConsoleStore
{
    public static class MockDbSetFactory
    {
        public static Mock<DbSet<T>> Create<T>(IEnumerable<T> data) where T : class
        {
            var queryData = data.AsQueryable();
            var mockDbSet = new Mock<DbSet<T>>();
            mockDbSet.As<IAsyncEnumerable<T>>()
                .Setup(d => d.GetAsyncEnumerator( new CancellationToken()))
                .Returns(new TestAsyncEnumerator<T>(queryData.GetEnumerator()));
            mockDbSet.As<IQueryable<T>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<T>(queryData.Provider));
            mockDbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryData.Expression);
            mockDbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryData.ElementType);
            mockDbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryData.GetEnumerator());

            return mockDbSet;
        }
    }
}
