﻿using Microsoft.Extensions.DependencyInjection;

namespace Cache
{
    public static class Bootstrap
    {
        public static IServiceCollection AddCache(this IServiceCollection services)
        {
            services.AddMemoryCache();
            return services;
        }
    }
}
