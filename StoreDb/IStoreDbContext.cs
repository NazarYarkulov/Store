﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StoreDb.Entities;

namespace StoreDb
{
    public interface IStoreDbContext : IDisposable
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Good> Products { get; set; }
        public DbSet<Order> Orders{ get; set; }

        Task<int> SaveChangesAsync(CancellationToken token = default);
    }
}
