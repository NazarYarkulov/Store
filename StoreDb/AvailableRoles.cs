﻿namespace StoreDb
{
    public static class AvailableRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
