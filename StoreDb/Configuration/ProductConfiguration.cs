﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StoreDb.Entities;

namespace StoreDb.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Good>
    {
        public void Configure(EntityTypeBuilder<Good> builder)
        {
            builder.HasKey(g => g.Id);
            builder.Property(g => g.Id).ValueGeneratedOnAdd();

            builder.Property(g => g.Description).HasMaxLength(2048);
            builder.Property(g => g.Name).HasMaxLength(256);

            
        }
    }
}
