﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StoreDb.Entities;

namespace StoreDb.Configuration
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).ValueGeneratedOnAdd();

            builder.Property(u => u.Username)
                .HasMaxLength(256);

            builder.Property(u => u.Password)
                .HasMaxLength(16);

            builder.HasMany<UserRole>()
                .WithOne(ur => ur.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            builder.HasMany<Order>()
                .WithOne(o => o.User)
                .HasForeignKey(o=> o.UserId)
                .IsRequired();

        }
    }
}
