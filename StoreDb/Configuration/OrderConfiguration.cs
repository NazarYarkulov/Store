﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using StoreDb.Entities;
using StoreDb.Enums;

namespace StoreDb.Configuration
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(g => g.Id);
            builder.Property(g => g.Id).ValueGeneratedOnAdd();

            var converter = new ValueConverter<OrderStatus, string>(
                o => o.ToString(),
                o => (OrderStatus)Enum.Parse(typeof(OrderStatus), o));
            builder.Property(o => o.Status).HasConversion(converter);

            builder.HasMany<Good>()
                .WithOne(o => o.Order)
                .HasForeignKey(o => o.OrderId)
                .IsRequired();
        }
    }


}
