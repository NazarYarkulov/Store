﻿using System.Collections.Generic;

namespace StoreDb.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public ICollection<UserRole> UserRoles{ get; set; }
        public ICollection<Order> Orders{ get; set; }

    }
}
