﻿namespace StoreDb.Entities
{
    public class Good
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }

        public int? OrderId { get; set; }
        public Order Order { get; set; }
    }
}
