﻿using System;
using System.Collections.Generic;
using StoreDb.Enums;

namespace StoreDb.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime LastChangesDate { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public ICollection<Good> Products{ get; set; }
    }
}
