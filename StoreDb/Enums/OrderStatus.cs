﻿namespace StoreDb.Enums
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        PaymentReceived,
        Sent,
        Received,
        Completed
    }
}
