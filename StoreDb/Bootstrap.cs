﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace StoreDb
{
    public static class Bootstrap
    {
        public static IServiceCollection AddStoreDb(this IServiceCollection services)
        {
            services.AddDbContext<IStoreDbContext, StoreDbContext>(config =>
             {
                 config.UseInMemoryDatabase("storedb");
             });

            services.AddTransient<MockDataInitializer>();
            return services;
        }
    }
}
