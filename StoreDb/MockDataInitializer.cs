﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StoreDb.Entities;

namespace StoreDb
{
    public class MockDataInitializer
    {
        private readonly IStoreDbContext _dbContext;

        public MockDataInitializer(IStoreDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddMockDataAsync()
        {
            var products = new List<Good>
            {
                new Good
                {
                    Id = 1,
                    Name = "Apple MacBook Air 13\" 256GB 2020 Gold (MWTL2)",
                    Category = "Ноутбуки",
                    Price = 29999,
                    Description =
                        @"Экран 13.3' IPS (2560x1600), глянцевый / Intel Core i3 (1.1 - 3.2 ГГц) / RAM 8 ГБ / SSD 256 ГБ / 
                        Intel Iris Plus Graphics / Wi-Fi / Bluetooth / macOS Catalina / 1.29 кг / золотой"
                },
                new Good
                {
                    Id = 2,
                    Name = "Asus X509FL-BQ293 (90NB0N12-M03830) Slate Grey",
                    Category = "Ноутбуки",
                    Price = 16999,
                    Description =
                        @"Экран 15.6' IPS (1920x1080) Full HD, матовый / Intel Core i5-8265U (1.6 - 3.9 ГГц) / RAM 8 ГБ / SSD 256 ГБ /
                           nVidia GeForce MX250, 2 ГБ / без ОД / Wi-Fi / Bluetooth / веб-камера / без ОС / 1.8 кг / серый"
                }
            };
            var roles = new List<Role>
            {
                new Role {Id = 1, Name = AvailableRoles.Admin},
                new Role {Id = 2, Name = AvailableRoles.User},
            };
            var users = new List<User>
            {
                new User {Id = 1, Username = "admin", Password = "admin"},
                new User {Id = 2, Username = "user", Password = "user"},
                new User {Id = 3, Username = "nazar", Password = "user"},
                new User {Id = 4, Username = "bogdan", Password = "user"},
                new User {Id = 5, Username = "marina", Password = "user"},
            };
            var userRoles = new List<UserRole>
            {
                new UserRole {UserId = 1, RoleId = 1},
                new UserRole {UserId = 2, RoleId = 2},
                new UserRole {UserId = 3, RoleId = 2},
                new UserRole {UserId = 4, RoleId = 2},
                new UserRole {UserId = 5, RoleId = 2},
            };

            _dbContext.Users.AddRange(users);
            _dbContext.Roles.AddRange(roles);
            _dbContext.UserRoles.AddRange(userRoles);
            _dbContext.Products.AddRange(products);

            await _dbContext.SaveChangesAsync();
        }
    }
}
