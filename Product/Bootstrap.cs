﻿using Microsoft.Extensions.DependencyInjection;

namespace Product
{
    public static class Bootstrap
    {
        public static IServiceCollection AddProduct(this IServiceCollection services)
        {
            services.AddTransient<IProductService, ProductService>();
            return services;
        }
    }
}
