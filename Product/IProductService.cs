﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StoreDb.Entities;

namespace Product
{
    public interface IProductService
    {
        Task<IList<Good>> GetAllAsync();
        Task<Good> GetAsync(string name);
        Task<Good> GetAsync(int id);
        Task AddAsync(Good product, CancellationToken token = default);
        Task ChangeAsync(Good updatedProduct);
    }
}
