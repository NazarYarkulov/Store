﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StoreDb;
using StoreDb.Entities;

namespace Product
{
    public class ProductService : IProductService
    {
        private readonly IStoreDbContext _dbContext;
        public ProductService(IStoreDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task AddAsync(Good product, CancellationToken token = default)
        {
            _dbContext.Products.Add(product);
            await _dbContext.SaveChangesAsync(token);
        }

        public async Task ChangeAsync(Good updatedProduct)
        {
            _dbContext.Products.Update(updatedProduct);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<IList<Good>> GetAllAsync() =>
            await _dbContext.Products.ToListAsync();

        public async Task<Good> GetAsync(string name) =>
            await _dbContext.Products
                .FirstOrDefaultAsync(p => p.Name.ToLower().Contains(name.ToLower()));
        public async Task<Good> GetAsync(int id) =>
             await _dbContext.Products.FirstOrDefaultAsync(p => p.Id == id);
    }
}
