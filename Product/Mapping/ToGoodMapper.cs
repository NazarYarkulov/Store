﻿using StoreDb.Entities;

namespace Product.Mapping
{
    public static class ToGoodMapper
    {
        public static Good ToGood(this Model.Product product) =>
            new Good
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Category = product.Category,
                Description = product.Description,
                OrderId = null
            };
    }
}
