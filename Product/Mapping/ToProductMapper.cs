﻿namespace Product.Mapping
{
    public static class ToProductMapper
    {
        public static Model.Product ToProduct(this StoreDb.Entities.Good good) =>
            new Model.Product()
            {
                Id = good.Id,
                Name = good.Name,
                Price = good.Price,
                Category = good.Category,
                Description = good.Description
            };
    }
}
